<?php
session_start();
require_once __DIR__ . '/vendor/autoload.php';
//require_once __DIR__ . '/vendor/facebook/php-sdk-v4/autoload.php';

use Facebook\FacebookApp;
use Facebook\FacebookSession;
use Facebook\Facebook;

//require_once __DIR__ . '/src/Facebook/autoload.php'; // download official fb sdk for php @ https://github.com/facebook/php-graph-sdk

$fb = new Facebook([
    'app_id' => '315141265640440',
    'app_secret' => '9689286067e0dddeb1be0eee6c88d0fc',
    'default_graph_version' => 'v2.2',
  ]);

$helper = $fb->getRedirectLoginHelper();
$permissions = ['user_birthday', 'user_location', 'user_website']; // optional
$loginUrl = $helper->getLoginUrl('http://simple.app/fb-callback.php', $permissions);
echo '<a href="' . htmlspecialchars($loginUrl) . '">Log in with Facebook!</a>';

