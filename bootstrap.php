<?php
require 'vendor/autoload.php';

use Illuminate\Database\Capsule\Manager as Capsule;

$capsule = new Capsule();
$capsule->addConnection([
    "driver"     => "mysql",
    "host"       => "localhost",
    "database"   => "faceapp",
    "username"   => "root",
    "password"   => "mysql777D",
    "charset"    => "utf8",
    "collation"  => "utf8_general_ci"
]);

$capsule->bootEloquent();